variable "database_region" {
  type        = string
  description = "Databases region"
}

variable "dashboard_name" {
  type        = string
  description = "Name of the RDS cloudwatch Dashboard"
  default     = "RDS"
}

variable "database_identifiers" {
  type        = list(string)
  description = "List of RDS database identifiers present in dashboard"
}

variable "database_max_simultaneous_connections_warning" {
  type        = number
  description = "Annotation value to show when there is too much simultaneous connections"
  default     = 70
}

variable "database_free_storage_space_warning" {
  type        = number
  description = "Annotation value to show when database is close to to have no storage"
  default     = 3000000000
}
