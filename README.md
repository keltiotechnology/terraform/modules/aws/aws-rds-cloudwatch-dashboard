# Aws rds cloudwatch dashboard

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_dashboard.rds](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_dashboard) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_dashboard_name"></a> [dashboard\_name](#input\_dashboard\_name) | Name of the RDS cloudwatch Dashboard | `string` | `"RDS"` | no |
| <a name="input_database_free_storage_space_warning"></a> [database\_free\_storage\_space\_warning](#input\_database\_free\_storage\_space\_warning) | Annotation value to show when database is close to to have no storage | `number` | `3000000000` | no |
| <a name="input_database_identifiers"></a> [database\_identifiers](#input\_database\_identifiers) | List of RDS database identifiers present in dashboard | `list(string)` | n/a | yes |
| <a name="input_database_max_simultaneous_connections_warning"></a> [database\_max\_simultaneous\_connections\_warning](#input\_database\_max\_simultaneous\_connections\_warning) | Annotation value to show when there is too much simultaneous connections | `number` | `70` | no |
| <a name="input_database_region"></a> [database\_region](#input\_database\_region) | Databases region | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
