resource "aws_cloudwatch_dashboard" "rds" {
  dashboard_name = var.dashboard_name

  dashboard_body = templatefile("${path.module}/templates/rds_dashboard.tpl", {
    database_identifiers                          = var.database_identifiers
    database_max_simultaneous_connections_warning = var.database_max_simultaneous_connections_warning
    database_free_storage_space_warning           = var.database_free_storage_space_warning
    database_region                               = var.database_region
    }
  )
}
