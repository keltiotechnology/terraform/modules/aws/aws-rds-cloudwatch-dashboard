{
    "widgets": [
        {
            "height": 6,
            "width": 6,
            "y": 6,
            "x": 0,
            "type": "metric",
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "DatabaseConnections", "DBInstanceIdentifier", "${identifier}", { "period": 300, "id": "m${index(database_identifiers, identifier)}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "view": "timeSeries",
                "stacked": false,
                "region": "${database_region}",
                "stat": "Average",
                "title": "Connexions simultanées",
                "period": 300,
                "annotations": {
                    "horizontal": [
                        {
                            "label": "Max",
                            "value": ${database_max_simultaneous_connections_warning}
                        }
                    ]
                }
            }
        },
        {
            "type": "metric",
            "x": 6,
            "y": 6,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "CPUUtilization", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}", "region": "${database_region}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "timeSeries",
                "stacked": false,
                "start": "-PT3H",
                "end": "P0D",
                "title": "Toutes les ressources - CPUUtilization",
                "region": "${database_region}"
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 0,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "FreeStorageSpace", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "bar",
                "stacked": false,
                "title": "Toutes les ressources - FreeStorageSpace",
                "region": "${database_region}",
                "annotations": {
                    "horizontal": [
                        {
                            "label": "Attention",
                            "value": ${database_free_storage_space_warning},
                            "fill": "below"
                        }
                    ]
                }
            }
        },
        {
            "type": "metric",
            "x": 0,
            "y": 12,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "FreeableMemory", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "timeSeries",
                "stacked": false,
                "start": "-PT3H",
                "end": "P0D",
                "title": "Toutes les ressources - FreeableMemory",
                "region": "${database_region}"
            }
        },
        {
            "type": "metric",
            "x": 18,
            "y": 0,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "WriteIOPS", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}", "region": "${database_region}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "timeSeries",
                "stacked": false,
                "start": "-PT3H",
                "end": "P0D",
                "title": "Toutes les ressources - WriteIOPS",
                "region": "${database_region}"
            }
        },
        {
            "type": "metric",
            "x": 12,
            "y": 6,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "WriteLatency", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "timeSeries",
                "stacked": false,
                "start": "-PT3H",
                "end": "P0D",
                "title": "Toutes les ressources - WriteLatency",
                "region": "${database_region}"
            }
        },
        {
            "type": "metric",
            "x": 6,
            "y": 0,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "ReadLatency", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}", "region": "${database_region}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "timeSeries",
                "stacked": false,
                "start": "-PT3H",
                "end": "P0D",
                "title": "Toutes les ressources - ReadLatency",
                "region": "${database_region}"
            }
        },
        {
            "type": "metric",
            "x": 12,
            "y": 0,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "ReadIOPS", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}", "region": "${database_region}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "timeSeries",
                "stacked": false,
                "start": "-PT3H",
                "end": "P0D",
                "title": "Toutes les ressources - ReadIOPS",
                "region": "${database_region}"
            }
        },
        {
            "type": "metric",
            "x": 18,
            "y": 6,
            "width": 6,
            "height": 6,
            "properties": {
                "metrics": [
                    %{ for identifier in database_identifiers ~}
                    [ "AWS/RDS", "WriteThroughput", "DBInstanceIdentifier", "${identifier}", { "stat": "Average", "id": "m${index(database_identifiers, identifier)}", "region": "${database_region}" } ]%{if index(database_identifiers, identifier) != length(database_identifiers) - 1},%{endif}
                    %{ endfor ~}
                ],
                "legend": {
                    "position": "bottom"
                },
                "period": 300,
                "view": "timeSeries",
                "stacked": false,
                "start": "-PT3H",
                "end": "P0D",
                "title": "Toutes les ressources - WriteThroughput",
                "region": "${database_region}"
            }
        }
    ]
}
